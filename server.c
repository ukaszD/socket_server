#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <zconf.h>

#define PORT 6666
#define BUFFER_SIZE 1024

int main() {
    int server_fd;
    int new_socket;
    struct sockaddr_in address;
    char data_buffer[BUFFER_SIZE] = {0};
    int opt = 1;
    int addrlen = sizeof(address);

    printf("Server started...\n");
    printf("Waiting for connection...\n");
    printf("=========================\n");

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("!!!Socket error!!!\n");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("!!!Set socket error!!!\n");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *) &address, sizeof(address)) < 0) {
        perror("!!!Bind error!!!\n");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 3) < 0) {
        perror("!!!Listen error!!!\n");
        exit(EXIT_FAILURE);
    }
    if ((new_socket = accept(server_fd, (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0) {
        perror("!!!Accept error!!!\n");
        exit(EXIT_FAILURE);
    }

    do {
        bzero(data_buffer, BUFFER_SIZE);
        if (read(new_socket, data_buffer, BUFFER_SIZE - 1) < 0) {
            perror("!!!Reading error!!!\n");
            break;
        }

        if (strncmp("exit", data_buffer, 2) == 0) {
            perror("!!!Client disconnected!!!\n");
            break;
        }

        if (strncmp("--connect_request--", data_buffer, 2) == 0) {
            printf("<client connected>\n");
            bzero(data_buffer, BUFFER_SIZE);
        }

        printf("[Client]: %s\n", data_buffer);
        bzero(data_buffer, BUFFER_SIZE);
        printf("> ");
        scanf("%s", data_buffer);

        if (write(new_socket, data_buffer, BUFFER_SIZE - 1) < 0) {
            perror("!!!Sending error!!!\n");
            break;
        }
    }
    while (strncmp("exit", data_buffer, 2) != 0);


    close(new_socket);
    return 0;
}

